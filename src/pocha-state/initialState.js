var testState = {
  'players': [
    {
      'portrait': null,
      'id': -1,
      'order': -1,
      'color': 'red',
    },
    {
      'portrait': './favicon.ico',
      'id': -2,
      'order': -2,
      'color': 'green',
    },
    {
      'portrait': './add_user.png',
      'id': -3,
      'order': -3,
      'color': 'blue',
    },
  ],
  'scoring': {
    'round': 3,
    'score': {
      [-1]: {current:10, tips:2},
      [-2]: {current:5, tips:1},
      [-3]: {current:10, tips:-1},
    },
  },
  'rules': {
    'ruleset': 'pocheada',
    'roundflags': ['x2'],
    'flags': [],
  },
  'screen': {
    'presentation': 'setup',
    'graph': null,
  },
};

var initialState = testState;