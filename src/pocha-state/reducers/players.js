const players = (state = [], action) => {
  
  switch (action.type) {
    case 'SETUP_PLAYER_ADD':
      let id = _nextId(state);
      let color = _nextColor(id);
      return [
        ...state,
        {
          portrait: null,
          id,
          order: id,
          color,
        },
      ];
    case 'SETUP_PLAYER_REMOVE':
      return state.filter((player) => player.id !== action.player);
    case 'SETUP_PLAYER_SET_PORTRAIT':
      return state.map((player) => player.id === action.player ?
        Object.assign({}, player, {portrait: action.src}) :
        player );
    default:
      return state;
  }

  function _nextId(players){
    if (players.length){
      var max = (Array.from(players))
      .map(player => {return player.id})
      .reduce((acc,id)=>{return acc>id?acc:id},1);
      return max + 1;
    }
    else return 1;
  }

  function _nextColor(id){
    let avaliableColors=['red', 'blue', 'green', 'pink', 'orange', 'cyan', 'brown', 'yellow', 'magenta'];
    return avaliableColors[id % avaliableColors.length];
  }
};
