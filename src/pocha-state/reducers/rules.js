const rules = (state = {ruleset: 'pocha', roundflags: [], flags: []}, action) => {
  switch (action.type) {
    case 'RULE_SET' :  // rule: string
      return {ruleset: action.rule, roundflags: [], flags: []};
    case 'RULE_ROUND_FLAGS': // flags: array
      return Object.assign({}, state, {roundflags: action.flags});
    case 'ADD_ROUND':
      return Object.assign({}, state, {roundflags: []});
    default:
      return state;
  }
};

