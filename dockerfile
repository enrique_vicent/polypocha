FROM node:6
LABEL repository: "evicent/polymerterster:2.0" 

EXPOSE  80 8080 8081 3000 5001 8000

RUN apt-get update && apt-get install -y \
  wget \
  --no-install-recommends \
  && wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
  && apt-get update && apt-get install -y \
  google-chrome-stable \
  --no-install-recommends \
  && apt-get install -y default-jre \
  && rm -rf /var/lib/apt/lists/*

RUN npm install -g polymer-cli bower