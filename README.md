# Publication

This app is accesible at https://polypocha.bitbucket.io .

This app is an evolution https://micropocha.bitbucket.io

Publication is performed by automatic building


# Build

docker runnin environment with

```bash
docker run -dti --name nodedev -p 80:80 -p 8080:8080 -p 8081:8081 -p 3000:3000 -p 5001:5001 -v $PWD:/app node:6 bash

docker attach nodedev
npm install -g browser-sync bower polymer-cli
```

## to dev-run

```bash
npm run watch &
polymer serve &
polymer test -l chrome -p 
```

app reloads at: http://localhost:8080/
tests reloads at: http://localhost:8080/test/index.html 


## before push you should do:

```bash
polymer test -l firefox -l chrome && polymer build
```

## create the docker image for pipeline

to accelerate the build process a docker image is built:

```bash
docker build -t evicent/polymerterster:2.1 . 
docker push evicent/polymerterster:2.1
```

# resources

- [flex layout reference card](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [redux](https://www.captaincodeman.com/2017/07/19/project-structure-for-using-redux-with-polymer-20)
- [testing with chrome headless](https://ncona.com/2015/12/running-polymer-tests-with-docker/)
- [using reselect to create redux-selectors](https://github.com/reactjs/reselect)
- generating themes
 - [paper](https://polymerthemes.com/) 
 - [material](https://www.materialpalette.com/brown/orange)
- [gradient generator](https://www.cssmatic.com/es/gradient-generator#)